import './App.css';
import "bootstrap/dist/css/bootstrap.min.css"
import React from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom"
import Login from './components/login/Login';
import useToken from './useToken';
import { SuccessLogin } from './components/successlogin/SuccessLogin';

function App() {

  const { token, setToken } = useToken();

  if(!token) {
    return <Login setToken={setToken} />
  }

  return (
  <BrowserRouter>
    <Routes>
      <Route path="/login" element={<Login />} />
      <Route path="/successlogin" element={<SuccessLogin />} />
    </Routes>
  </BrowserRouter>

  );
}

export default App;
