import React, { useState } from 'react'
import PropTypes from 'prop-types';

async function loginUser(credentials) {
  const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(credentials) 
  };

  return fetch(`http://localhost:8090/api/v1/auth/login`, requestOptions)
  .then(response => {

      if (!response.ok) {
          const error = response.status;
          
          if(response.status === 401)
          {
            alert("Login or password is invalid! Check your credentials");
          }
      } 
      return response.json();
    })
  .catch(error => {
      console.error('There was an error!', error);
  });
 }
 

export default function Login({ setToken }) {
  
  const [username, setUserName] = useState('70000000000');
  const [password, setPassword] = useState("Qwerty123");

  const handleSubmit = async e => {
    e.preventDefault();
    const token = await loginUser({
      login: username,
      password : password
    });
    setToken(token);
  }

  return (
    <div className="Auth-form-container">
      <form className="Auth-form" onSubmit={handleSubmit}>
        <div className="Auth-form-content">
          <h3 className="Auth-form-title">Sign In</h3>
          <div className="form-group mt-3">
            <label>Username</label>
            <input
              type="text"
              className="form-control mt-1"
              placeholder="Enter username"
              onChange={e => setUserName(e.target.value)}
            />
          </div>
          <div className="form-group mt-3">
            <label>Password</label>
            <input
              type="password"
              className="form-control mt-1"
              placeholder="Enter password"
              onChange={e => setPassword(e.target.value)}
            />
          </div>
          <div className="d-grid gap-2 mt-3">
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </div>
        </div>
      </form>
    </div>
  )
}

Login.propTypes = {
  setToken: PropTypes.func.isRequired
}

